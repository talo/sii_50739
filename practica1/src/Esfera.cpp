// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


//CODIGO MODIFICADO POR GONZALO DEL VALLE 50739//

Esfera::Esfera()
{
	radio=0.5f;
	centro.x=0;
	centro.y=0;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro.x=centro.x+velocidad.x*t;
centro.y= centro.y+velocidad.y*t; 
}

void Esfera::Decrece(float t)
{
	radio =radio-0.8*t;
	if(radio<0.0) radio=1;
}
